//
//  LoginViewModel.swift
//  Sertifikasi_Bevan
//
//  Created by bevan christian on 07/05/22.
//

import Foundation
import SwiftUI
import Combine


class LoginViewModel: ObservableObject {
    @Published var email: String = ""
    @Published var emptyState: Bool = false
    @Published var password: String = ""
    private let coreService = CoreDataService.shared
    var subscription = Set<AnyCancellable>()
    
    init() {
        validationLogin()
    }
    
    
    func login(completion: @escaping() -> Void) {
        if emptyState {
            coreService.insertLogin(email: email, password: password)
            setUserDefault()
            completion()
        }
    }
    
    func validationLogin() {
        $email.combineLatest($password).sink { [self] ema, pass in
            if ema.count < 5  || pass.count < 8 {
                emptyState = false
            } else {
                emptyState = true
            }
        }.store(in: &subscription)
    }
    
    func setUserDefault() {
        UserDefaults.standard.set(true, forKey: "NewUser")
        UserDefaults.standard.set(email, forKey: "email")
        UserDefaults.standard.set(password, forKey: "password")
    }
    
}
