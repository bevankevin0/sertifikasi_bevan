//
//  AddViewModel.swift
//  Sertifikasi_Bevan
//
//  Created by bevan christian on 07/05/22.
//

import Foundation
import UIKit
import Combine



class BooksViewModel: ObservableObject {
    @Published var dataBuku: BukuLocal = BukuLocal(nama: "")
    @Published var nama = ""
    @Published var itemsContentPicOrVid = PhotoPickerModel(with: UIImage(named: "placeholder")!)
    
    private let coreService = CoreDataService.shared
    
    func append(item: PhotoPickerModel) {
        itemsContentPicOrVid = item
        dataBuku.gambar = item.photo
    }
    
    func addBook() {
        coreService.insertBook(book: BukuLocal(id: UUID(), idBuku: "\(UUID())", nama: nama, status: false, gambar: itemsContentPicOrVid.photo))
    }
    
    func updateBook(book: BukuLocal) {
        coreService.updateBook(book)
    }
    
    func deleteBook(idBook: String) {
        coreService.delete(idBook)
    }
}
