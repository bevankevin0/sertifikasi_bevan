//
//  BookingViewModel.swift
//  Sertifikasi_Bevan
//
//  Created by bevan christian on 08/05/22.
//

import Foundation
import Combine

class BookingViewModel: ObservableObject {
    @Published var bookingData = [Invoice]()
    private let coreService = CoreDataService.shared
    private var subscription = Set<AnyCancellable>()
    
    func getBookingData() {
        coreService.getListBooking().sink { completion in
            switch completion {
            case .finished:
                print("finfished")
            case .failure(let error):
                print(error.localizedDescription)
            }
        } receiveValue: { items in
            self.bookingData = items
        }.store(in: &subscription)
    }
    
    func returnBook() {
        coreService.getReturnToday()
    }
    
}
