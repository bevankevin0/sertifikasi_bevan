//
//  HomeViewModel.swift
//  Sertifikasi_Bevan
//
//  Created by bevan christian on 03/05/22.
//

import Foundation
import Combine
import SwiftUI

class HomeViewModel: ObservableObject {
    @Published var books = [BukuLocal]()
    private var subscription = Set<AnyCancellable>()
    private let coreService = CoreDataService.shared
    @Published var itemsContentPicOrVid = PhotoPickerModel(with: UIImage(named: "placeholder")!)
    
    
    func append(item: PhotoPickerModel) {
        itemsContentPicOrVid = item
    }
    
    func getData() {
        coreService.getBook().sink { completion in
            switch completion {
            case .finished:
                print("finfished")
            case .failure(let error):
                print(error.localizedDescription)
            }
        } receiveValue: { items in
            self.books = items
        }.store(in: &subscription)
    }

    
    func borrowBook(books:[BukuLocal], namaPeminjam: String) {
        for book in books {
            coreService.borrowBook(book: book, namaPeminjam: namaPeminjam)
        }
    }
}
