//
//  Sertifikasi_BevanApp.swift
//  Sertifikasi_Bevan
//
//  Created by bevan christian on 03/05/22.
//

import SwiftUI
import Combine


@main
struct Sertifikasi_BevanApp: App {
    var body: some Scene {
        WindowGroup {
            MainView()
        }
    }
}
