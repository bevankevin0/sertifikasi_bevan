//
//  CoreDataService.swift
//  Sertifikasi_Bevan
//
//  Created by bevan christian on 03/05/22.
//

import Foundation
import CoreData
import Combine
import UIKit

class CoreDataService {
    static let shared = CoreDataService()
    private var container: NSPersistentContainer!
    
    
    init() {
        createPeristenceContainer()
    }
    
    private func createPeristenceContainer() {
        container = NSPersistentContainer(name: "Sertifikasi_Bevan")
        container.loadPersistentStores(completionHandler: { description, error in
            if error != nil {
                fatalError("erd not load")
            }
        })
    }
    
    func createContext() -> NSManagedObjectContext {
        return  container.viewContext
    }
    
    
    // books
    func insertLogin(email: String, password: String) {
        let context = createContext()
        context.performAndWait {
            let newData = User(context: context)
            newData.email = email
            newData.password = password
        }
        do {
            try context.save()
            print("saved")
        } catch  {
            print("error saved")
        }
    }
    
    func insertBook(book: BukuLocal) {
        let context = createContext()
        context.performAndWait {
            let newData = Buku(context: context)
            newData.status = true
            newData.nama_buku = book.nama
            newData.id_buku = UUID().uuidString
            newData.gambar = book.gambar?.jpegData(compressionQuality: 0.5)
        }
        do {
            try context.save()
            print("saved")
        } catch  {
            print("error saved")
        }
    }
    
    func borrowBook(book: BukuLocal, namaPeminjam: String) {
        let context = createContext()
        context.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        context.performAndWait {
            // peminjaman
            let newData = Peminjaman(context: context)
            newData.id_pinjam = UUID().uuidString
            newData.tgl_pinjam = Date()
            let sevenDayTommorow = Calendar.current.date(byAdding: .day, value: 7, to: Date())!
            newData.tgl_selesai = sevenDayTommorow
            // koleksi
            //            candy1.origin = Country(context: moc)
            newData.koleksi = Buku(context: context)
            newData.koleksi?.id_buku = book.idBuku
            newData.koleksi?.nama_buku = book.nama
            newData.koleksi?.gambar = book.gambar?.jpegData(compressionQuality: 0.5)
            newData.koleksi?.nama_buku = book.nama
            newData.koleksi?.status = book.status ?? false
            // user
            newData.peminjam = User(context: context)
            newData.peminjam?.email = namaPeminjam ?? "bevankevin0@gmail.com"
            newData.peminjam?.password = UserDefaults.standard.string(forKey: "password") ?? "bevan christian"
            
        }
        do {
            try context.save()
            updateBookStatus(books: book)
            print("saved")
        } catch  {
            print("error saved")
        }
    }
    
    private func updateBookStatus(books: BukuLocal) {
        let context = createContext()
        context.automaticallyMergesChangesFromParent = true
        context.performAndWait {
            let request: NSFetchRequest<Buku> = Buku.fetchRequest()
            if let idBuku = books.idBuku {
                request.predicate = NSPredicate(format: "\(#keyPath(Buku.id_buku)) == %@", idBuku)
                if let yangMauDiUpdate = try? context.fetch(request), let member = yangMauDiUpdate.first {
                    member.status = false
                    do {
                        try context.save()
                        print("update succed")
                    } catch  {
                        print("error saved")
                    }
                } else {
                    print("gae opo")
                }
            }
        }
    }
    
    func getBook() -> Future<[BukuLocal],Error> {
        return Future() { [self] promise in
            let context = createContext()
            context.automaticallyMergesChangesFromParent = true
            context.performAndWait {
                let request:NSFetchRequest<Buku> = Buku.fetchRequest()
                request.predicate = NSPredicate(format: "\(#keyPath(Buku.status)) == %@", NSNumber(value: true))
                do {
                    let result = try context.fetch(request)
                    var itemLocal = [BukuLocal]()
                    for result in result {
                        let data = BukuLocal(id: UUID(), idBuku: result.id_buku, nama: result.nama_buku ?? "", status: result.status, gambar: UIImage(data: result.gambar ?? Data()))
                        itemLocal.append(data)
                    }
                    promise(.success(itemLocal))
                } catch  {
                    promise(.failure(error))
                }
            }
        }
    }
    
    func updateBook (_ books: BukuLocal) {
        let context = createContext()
        context.automaticallyMergesChangesFromParent = true
        context.performAndWait {
            let request: NSFetchRequest<Buku> = Buku.fetchRequest()
            if let idBuku = books.idBuku {
                request.predicate = NSPredicate(format: "\(#keyPath(Buku.id_buku)) == %@", idBuku)
                if let yangMauDiUpdate = try? context.fetch(request), let member = yangMauDiUpdate.first {
                    member.nama_buku = books.nama
                    member.gambar = books.gambar?.jpegData(compressionQuality: 0.5)
                    do {
                        try context.save()
                        print("update succed")
                    } catch  {
                        print("error saved")
                    }
                } else {
                    print("gae opo")
                }
            }
        }
    }
    
    func delete(_ id:String) {
        let context = createContext()
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Buku")
        fetchRequest.predicate = NSPredicate(format: "\(#keyPath(Buku.id_buku)) == %@", id)
        let delete = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        delete.resultType = .resultTypeCount
        if let delete = try? context.execute(delete) as? NSBatchDeleteResult {
            if delete.result != nil {
                print("deleted")
            } else {
                print("not deleted")
            }
        }
    }
    
    
    func getListBooking() -> Future<[Invoice],Error> {
        return Future() { [self] promise in
            let context = createContext()
            context.automaticallyMergesChangesFromParent = true
            context.performAndWait {
                let request:NSFetchRequest<Peminjaman> = Peminjaman.fetchRequest()
                do {
                    let result = try context.fetch(request)
                    var invoice = [Invoice]()
                    for result in result {
                        let data = Invoice(idBuku: result.koleksi?.id_buku ?? "", namaBuku: result.koleksi?.nama_buku ?? "placeholder", tanggalPinjam: result.tgl_pinjam ?? Date(), tglSelesaiPinjam: result.tgl_selesai ?? Date(), peminjamNama: result.peminjam?.email ?? "email")
                        invoice.append(data)
                        print("list data \(invoice)")
                    }
                    promise(.success(invoice))
                } catch  {
                    promise(.failure(error))
                }
            }
        }
    }
    
    func getReturnToday() {
        let context = createContext()
        context.automaticallyMergesChangesFromParent = true
        context.performAndWait {
            let request: NSFetchRequest<Peminjaman> = Peminjaman.fetchRequest()
            if let result = try? context.fetch(request) {
                for result in result {
                    if result.tgl_selesai ?? Date() <= Date() {
                        updateBookStatus(booksId: result.koleksi?.id_buku ?? "")
                        deletePeminjaman(result.id_pinjam ?? "")
                        
                    }
                }
            } else {
                print("gae opo")
            }
            
        }
    }
    
    func deletePeminjaman(_ id:String){
            let coreService = CoreDataService.shared
            let context = coreService.container.viewContext
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Peminjaman")
            fetchRequest.predicate = NSPredicate(format: "\(#keyPath(Peminjaman.id_pinjam)) == %@", id)
            let delete = NSBatchDeleteRequest(fetchRequest: fetchRequest)
            delete.resultType = .resultTypeCount
            if let delete = try? context.execute(delete) as? NSBatchDeleteResult {
                if delete.result != nil {
                    print("selesai delete")
                } else {
                    print("gagal delete")
                }
            }
    }
    
    
    
    func updateBookStatus(booksId: String) {
        let context = createContext()
        context.automaticallyMergesChangesFromParent = true
        context.performAndWait {
            let request: NSFetchRequest<Buku> = Buku.fetchRequest()
                request.predicate = NSPredicate(format: "\(#keyPath(Buku.id_buku)) == %@", booksId)
                if let yangMauDiUpdate = try? context.fetch(request), let member = yangMauDiUpdate.first {
                    member.status = true
                    do {
                        try context.save()
                        print("update succed")
                    } catch  {
                        print("error saved")
                    }
                } else {
                    print("gae opo")
                }
        }
    }
    
    
    
    
    
}
