//
//  HomepageView.swift
//  Sertifikasi_Bevan
//
//  Created by bevan christian on 07/05/22.
//

import SwiftUI
import Combine

struct HomepageView: View {
    @State var bookSelection = Set<BukuLocal>()
    @State private var isEditMode: EditMode = .inactive
    @State private var addState = false
    @State private var namaPeminjam = "Bevan"
    @StateObject var homeVm = HomeViewModel()
    var body: some View {
        VStack {
            NavigationView {
                VStack {
                    TextfieldComponentView(value: $namaPeminjam, title: "Nama Peminjam", prompt: "Masukan Nama peminjam", footer: "")
                    List(homeVm.books,id:\.self,selection: $bookSelection) { data in
                        NavigationLink {
                            DetailBookView(dataBuku: data)
                        } label: {
                            VStack {
                                HStack {
                                    Image(uiImage: (data.gambar ?? UIImage(named: "placeholder"))!)
                                        .resizable()
                                        .frame(width: 140, height: 140)
                                    VStack {
                                        Text("Nama Buku: \(data.nama)")
                                        if data.status == true {
                                            Text("Status : Bisa Dipinjam")
                                        } else {
                                            Text("Status : Tidak Bisa Dipinjam")
                                        }
                                    }
                                }
                            }
                        }
                    }
                    .refreshable {
                        homeVm.getData()
                    }
                    .listStyle(.plain)
                    .navigationBarItems(trailing: HStack {
                        EditButton()
                        Button {
                            addState.toggle()
                        } label: {
                            Text("Add")
                        }
                        
                    })
                    .sheet(isPresented: $addState, onDismiss: {
                        homeVm.getData()
                    }, content: {
                        AddView(addState: $addState)
                    })
                    .environment(\.editMode, self.$isEditMode)
                }
                .navigationBarBackButtonHidden(true)
                .onAppear {
                    homeVm.getData()
                }
                .onChange(of: $isEditMode.wrappedValue, perform: { value in
                  if value.isEditing {
                     // Entering edit mode (e.g. 'Edit' tapped)
                      bookSelection.removeAll()
                  } else {
                      homeVm.borrowBook(books: Array(bookSelection), namaPeminjam: namaPeminjam)
                  }
                })
            }
            
        }.navigationBarTitleDisplayMode(.inline)
            .navigationBarHidden(true)
            .navigationBarBackButtonHidden(true)
    }

}

struct HomepageView_Previews: PreviewProvider {
    static var previews: some View {
        HomepageView()
    }
}
