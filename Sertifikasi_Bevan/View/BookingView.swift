//
//  BookingView.swift
//  Sertifikasi_Bevan
//
//  Created by bevan christian on 08/05/22.
//

import SwiftUI

struct BookingView: View {
    @StateObject var bookingVm = BookingViewModel()
    var body: some View {
        VStack {
            if bookingVm.bookingData.isEmpty {
                LottieView(name: "bingung", loopMode: .loop)
                    .frame(width: 200, height: 200)
            } else {
                List(bookingVm.bookingData) { data in
                    VStack {
                        HStack {
                            VStack {
                                Text("Nama Buku: \(data.namaBuku)")
                                Text("tgl pinjam: \(data.tanggalPinjam)")
                                Text("tgl kembali: \(data.tglSelesaiPinjam)")
                                Text("peminjam: \(data.peminjamNama)")
                            }
                        }
                    }
                }
             
            }
        }.onAppear {
            bookingVm.getBookingData()
            bookingVm.returnBook()
        }
    }
}

struct BookingView_Previews: PreviewProvider {
    static var previews: some View {
        BookingView()
    }
}
