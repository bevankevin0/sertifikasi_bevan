//
//  LoginView.swift
//  Sertifikasi_Bevan
//
//  Created by bevan christian on 07/05/22.
//

import SwiftUI

struct LoginView: View {
    @StateObject var loginVm = LoginViewModel()
    @State var loginFinished = false
    var body: some View {
        NavigationView {
            VStack {
                NavigationLink(destination: MainView(), isActive: $loginFinished) {
                    EmptyView()
                }
                Text("Login Page")
                    .font(.title)
                Spacer()
                TextfieldComponentView(value: $loginVm.email, title: "Email", prompt: "Insert Email", footer: "minimum 5 huruf")
                TextfieldComponentView(value: $loginVm.password, isSecure: true, title: "Password", prompt: "Insert Password", footer: "minimum 8 hurruf")
                Spacer()
                Button {
                    if loginVm.emptyState {
                        loginVm.login {
                            loginFinished = true
                        }
                    }
                } label: {
                    Text("Login")
                        .frame(width: 200, height: 50)
                        .background(loginVm.emptyState ? Color.blue : Color.gray)
                        .foregroundColor(.white)
                        .cornerRadius(10)
                }
            }.padding()
        }
        .navigationBarTitleDisplayMode(.inline)
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}
