//
//  UpdateView.swift
//  Sertifikasi_Bevan
//
//  Created by bevan christian on 08/05/22.
//

import SwiftUI

struct UpdateView: View {
    @ObservedObject var bookVm:BooksViewModel
    @State var showGaleryPicker = false
    @Binding var updateState: Bool
    var body: some View {
        VStack {
            Button {
                showGaleryPicker = true
            } label: {
                Image(uiImage: bookVm.dataBuku.gambar!)
                    .resizable()
                    .scaledToFit()
                    .frame(width: 300, height: 200)
                    .background(Color(UIColor(red: 0.961, green: 0.961, blue: 0.961, alpha: 1)))
                    .cornerRadius(10)
            }
            TextfieldComponentView(value: $bookVm.dataBuku.nama, title: "Nama Buku", prompt: "Insert Book Name", footer: "")
            Spacer()
            Button {
                bookVm.updateBook(book: bookVm.dataBuku)
                updateState = false
            } label: {
                Text("Update")
                    .frame(width: 200, height: 50)
                    .background(Color.blue)
                    .foregroundColor(.white)
                    .cornerRadius(10)
            }
            .sheet(isPresented: $showGaleryPicker, content: {
                    PHPickerView(didFinishPicking: { didSelectItem in
                        showGaleryPicker = false
                    }, mediaItems: bookVm)
            })
        }.padding()
    }
    
}

struct UpdateView_Previews: PreviewProvider {
    static var previews: some View {
        UpdateView(bookVm: BooksViewModel(), updateState: .constant(false))
    }
}
