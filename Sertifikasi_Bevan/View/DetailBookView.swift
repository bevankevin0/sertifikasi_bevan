//
//  DetailBookView.swift
//  Sertifikasi_Bevan
//
//  Created by bevan christian on 07/05/22.
//

import SwiftUI
import Lottie

struct DetailBookView: View {
    @StateObject var bookVm = BooksViewModel()
    var dataBuku: BukuLocal
    @State var updateState = false
    var body: some View {
        VStack {
            NavigationLink(destination: UpdateView(bookVm: bookVm, updateState: $updateState), isActive: $updateState) {
                EmptyView()
            }
            Text("Nama : \((dataBuku.nama))")
            if dataBuku.status == true {
                Text("Status : Bisa Dipinjam")
            } else {
                Text("Status : Tidak Bisa Dipinjam")
            }
        }
        .onAppear(perform: {
            bookVm.dataBuku = dataBuku
        })
        .navigationBarItems(trailing:
                                HStack {
            Button(action: {
                updateState = true
            }, label: {
                Text("Update")
            })
            
            Button(action: {
                bookVm.deleteBook(idBook: dataBuku.idBuku ?? "")
            }, label: {
                Text("Delete")
            })
        }
        )
    }
}

struct DetailBookView_Previews: PreviewProvider {
    static var previews: some View {
        DetailBookView(dataBuku: BukuLocal(idBuku: "", nama: "", status: false, gambar: UIImage(named: "clipboard")))
    }
}
