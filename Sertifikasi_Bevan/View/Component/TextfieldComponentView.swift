//
//  TextfieldComponentView.swift
//  Sertifikasi_Bevan
//
//  Created by bevan christian on 07/05/22.
//

import SwiftUI

struct TextfieldComponentView: View {
    @Environment(\.colorScheme) var colorScheme
    @Binding var value: String
    var isSecure = false
    var title: String
    var prompt: String
    var footer: String
    var body: some View {
        VStack(alignment: .leading) {
            Text("\(title)")
                .font(.system(size: 14))
            if isSecure {
                SecureField("\(prompt)", text: $value)
                    .font(.system(size: 13))
                    .padding()
                    .background(Color(UIColor(red: 0.961, green: 0.961, blue: 0.961, alpha: 1)))
                    .foregroundColor(colorScheme == .dark ? Color.black : Color.black)
                    .cornerRadius(12)
            } else {
                TextField("\(prompt)", text: $value)
                    .font(.system(size: 13))
                    .padding()
                    .background(Color(UIColor(red: 0.961, green: 0.961, blue: 0.961, alpha: 1)))
                    .foregroundColor(colorScheme == .dark ? Color.black : Color.black)
                    .cornerRadius(12)
            }

            Text("\(footer)")
                .font(.system(size: 12))
        }.padding([.leading,.top,.trailing])
    }
}

struct TextfieldComponentView_Previews: PreviewProvider {
    static var previews: some View {
        TextfieldComponentView(value: .constant(""), title: "judul", prompt: "hello", footer: "footer")
    }
}
