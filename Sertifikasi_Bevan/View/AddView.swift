//
//  AddView.swift
//  Sertifikasi_Bevan
//
//  Created by bevan christian on 07/05/22.
//

import SwiftUI

struct AddView: View {
    @StateObject var addVm = BooksViewModel()
    @State var showGaleryPicker = false
    @Binding var addState: Bool
    var body: some View {
        VStack {
            Button {
                showGaleryPicker = true
            } label: {
                Image(uiImage: (addVm.itemsContentPicOrVid.photo ?? UIImage(named: "placeholder"))!)
                    .resizable()
                    .scaledToFit()
                    .frame(width: 300, height: 200)
                    .background(Color(UIColor(red: 0.961, green: 0.961, blue: 0.961, alpha: 1)))
                    .cornerRadius(10)
            }
            TextfieldComponentView(value: $addVm.nama, title: "Nama Buku", prompt: "Insert Book Name", footer: "")
            Spacer()
            Button {
                addVm.addBook()
                addState = false
            } label: {
                Text("Add")
                    .frame(width: 200, height: 50)
                    .background(Color.blue)
                    .foregroundColor(.white)
                    .cornerRadius(10)
            }
            .sheet(isPresented: $showGaleryPicker, content: {
                    PHPickerView(didFinishPicking: { didSelectItem in
                        showGaleryPicker = false
                    }, mediaItems: addVm)
            })
        }.padding()
    }
}

struct AddView_Previews: PreviewProvider {
    static var previews: some View {
        AddView(addState: .constant(false))
    }
}
