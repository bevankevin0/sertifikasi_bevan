//
//  MainView.swift
//  Sertifikasi_Bevan
//
//  Created by bevan christian on 08/05/22.
//

import SwiftUI
import Combine


struct MainView: View {
    var body: some View {
            if UserDefaults.standard.bool(forKey: "NewUser") == true
            {
                TabView {
                    HomepageView()
                        .tabItem {
                            VStack {
                                Image(systemName: "house.fill")
                                Text("Home")
                            }
                        }
                    BookingView()
                        .tabItem {
                            VStack {
                                Image(systemName: "bell.fill")
                                Text("Scheduled")
                            }
                        }
                }
            } else {
                LoginView()
            }
    }
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}
