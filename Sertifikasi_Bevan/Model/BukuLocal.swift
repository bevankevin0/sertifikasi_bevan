//
//  BukuLocal.swift
//  Sertifikasi_Bevan
//
//  Created by bevan christian on 07/05/22.
//

import Foundation
import UIKit


struct BukuLocal: Identifiable, Hashable {
    var id: UUID = UUID()
    var idBuku: String?
    var nama: String
    var status: Bool?
    var gambar: UIImage?
}
