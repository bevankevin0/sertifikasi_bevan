//
//  PeminjamanLocal.swift
//  Sertifikasi_Bevan
//
//  Created by bevan christian on 07/05/22.
//

import Foundation


struct PeminjamanLocal {
    var idPinjam: String?
    var tgl_pinjam: String?
    var tgl_selesai: String?
}
