//
//  InvoiceLocal.swift
//  Sertifikasi_Bevan
//
//  Created by bevan christian on 07/05/22.
//

import Foundation


struct Invoice: Hashable, Identifiable {
    var id = UUID()
    var idBuku: String
    var namaBuku: String
    var tanggalPinjam: Date
    var tglSelesaiPinjam: Date
    var peminjamNama: String
}
